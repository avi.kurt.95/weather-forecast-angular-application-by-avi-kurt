// Application Weater Data Instance Model.
export class WeaterData {

  //Instance Properties.
  name:string;  // City Name.
	temp:string;  // Current Temperature.
	lon:string;   // City Longitude.
  lat:string;   // City Latitude .


  //Default Constructor.
  constructor() {
    this.name = "";
    this.temp = "";
    this.lon  = "";
    this.lat  = "";
  }
}
