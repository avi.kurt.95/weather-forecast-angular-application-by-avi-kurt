import { WeaterData } from "./WeatherData";

// Application User Instance Model.
export class User {

  //Instance Properties.
  id: number;   // User's ID Number.
  user_name:string;  // User's Name.
	user_email:string; // User's Email Adress.
	user_password:string; // User's Password.
  users_сities: Array<WeaterData>;  // User's Chosen Cities.

  //Default Constructor.
  constructor() {
    this.id = 0;
    this.user_name = "";
    this.user_email = "";
    this.user_password = "";
    this.users_сities = [];
  }
}
