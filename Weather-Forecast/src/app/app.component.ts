import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './Models/User';
import { AuthenticationService } from './Services/Auentication Service/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

//Global Application Page View.
export class AppComponent {
  //Instance Attributes.
  title = 'Weather-Forecast';
  currentUser!: User;

  //Composed Constructor.
  constructor(
    //Services
    private router: Router,
    private authService: AuthenticationService,
  ) {
    //Attributes Initialization.
    this.authService.currentUser.subscribe(x => this.currentUser = x);
  }

  //Redirect to Users-Dashboard page.
  goToDashboard(){
    this.router.navigate(['../User/'+ this.currentUser.id+"/Dashboard"]);
  }

  //Redirect to Users-Settings page.
  goToSettings(){
    this.router.navigate(['../User/'+ this.currentUser.id+"/Settings"]);
  }

  //User Logout form the System.
  logout() {
    this.authService.logout();
    this.router.navigate(['Home-Page']);
  }
}
