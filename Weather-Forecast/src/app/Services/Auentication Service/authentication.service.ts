import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'src/app/Models/User';
import { JsonServerServiceService } from '../JSON Server API/json-server-service.service';
import { ActivatedRoute, Router } from '@angular/router';


@Injectable({ providedIn: 'root' })

//users Authentication Service.
export class AuthenticationService {

  //Instance Properties.
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  logged_user:User;

  //Composed Constructor.
  constructor(
    //Services.
    private jsonServer: JsonServerServiceService,
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    ) {
    //Attributes Iniliazation.
    this.logged_user = new User();
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')!));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  //Get Current Logged User Value.
  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  //Authenticate Current User.
  login(user_email:string, user_password:string) {
    this.jsonServer.getUserByEmailAndPassword(user_email,user_password)
    .subscribe(
          data =>{
            this.logged_user = data[0];

            if (this.logged_user == undefined){
              alert("User Not Found: Wrong password or Email.");
              this.logged_user = new User();

            }else{
              localStorage.setItem('currentUser', JSON.stringify(this.logged_user));
              this.currentUserSubject.next(this.logged_user);
              this.router.navigate(['../User', this.logged_user.id], {relativeTo: this.route});
            }
        },
        error=>{
          alert("Something Went Wrong. Please Try again later.");
        });
  }

  //Logout Current Logged User from the System.
  logout() {
    //remove user from local storage and set current user to null
    localStorage.clear();
    this.currentUserSubject.next(null!);
  }
}
