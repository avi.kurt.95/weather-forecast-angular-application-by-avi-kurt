//Global "Opne Weather" API Constants.
export class GlobalConstants {

  //Open Weather API Key.
  public static apiKey: string = "&appid=416f169c6c95bdaf9db2c1fa372fc9f0";

  //Open Weather Global API.
  public static globalApi: string = "https://api.openweathermap.org/data/2.5/weather?q=";
}
