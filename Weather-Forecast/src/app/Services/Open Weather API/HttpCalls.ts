import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalConstants } from './Global-Constants';

@Injectable({ providedIn: 'root' })

//OpenWeather HTTP JSON Call Module.
export class HttpCalls {

  //Default Constructor.
  constructor(
    //Services.
    private http: HttpClient
  ) { }

  //Get Single Wether Data by City Name.
  getOneCityData(city_name:string): Observable<any> {
    return this.http.get(`${GlobalConstants.globalApi}${city_name}${GlobalConstants.apiKey}`);
  }
}
