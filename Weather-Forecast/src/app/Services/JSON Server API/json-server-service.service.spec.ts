import { TestBed } from '@angular/core/testing';

import { JsonServerServiceService } from './json-server-service.service';

describe('JsonServerServiceService', () => {
  let service: JsonServerServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JsonServerServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
