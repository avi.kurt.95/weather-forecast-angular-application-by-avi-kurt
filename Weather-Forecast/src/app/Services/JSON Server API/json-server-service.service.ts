import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/Models/User';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

//JSON Server HTTP Servics.
export class JsonServerServiceService {

  constructor(
    private http:HttpClient
  ) { }

  //HTTP Post - Create User.
  createUser(new_user:User){
    return this.http.post<User>("http://localhost:3000/posts", new_user)
    .pipe(map((result:User)=>{
      return result;
    }))
  }

  //HTTP Get - Get Single User by ID.
  getUserById(id:number){
    return this.http.get<User>("http://localhost:3000/posts/"+id)
    .pipe(map((result:User)=>{
      return result;
    }))
  }

  //HTTP Get - Get Single User by Email & Password.
  getUserByEmailAndPassword(user_email:string, user_password:string){
    return this.http.get<any>("http://localhost:3000/posts?user_email="+user_email+"&user_password="+user_password)
    .pipe(map((result:any)=>{
      return result;
    }))
  }

  //HTTP Get - Get All Users.
  getAllUsers(){
    return this.http.get<User[]>("http://localhost:3000/posts")
    .pipe(map((result:User[])=>{
      return result;
    }))
  }

  //HTTP Put - Update User.
  updateUser(updated_user:User, id:number){
    return this.http.put<User>("http://localhost:3000/posts/"+id, updated_user)
    .pipe(map((result:any)=>{
      return result;
    }))
  }

  //HTTP Delete - Delete User.
  deleteUser(id:number){
    return this.http.delete<any>("http://localhost:3000/posts/"+id)
    .pipe(map((result:any)=>{
      return result;
    }))
  }
}
