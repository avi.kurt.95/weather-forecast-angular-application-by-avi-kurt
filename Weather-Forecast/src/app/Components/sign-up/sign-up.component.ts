import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/Models/User';
import { JsonServerServiceService } from 'src/app/Services/JSON Server API/json-server-service.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})

//Users Sighnup Page Component.
export class SignUpComponent implements OnInit {
  //Instance Attributes.
  signupForm!: FormGroup;
  returnUrl!: string;
  new_user: User = new User;
  submitted: boolean;

  //Composed Constructor.
  constructor(
    //Services.
    private jsonServer: JsonServerServiceService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,

  ) {
    //Attribute Initialization.
    this.new_user = new User();
    this.submitted = false;
    this.returnUrl = "";
  }

  //page on Init Method.
  ngOnInit(): void {
    // Patterns.
    const PAT_EMAIL = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+[.][a-zA-Z]{2,4}$";

    //Login Form Builder&Validators.
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email,Validators.pattern(PAT_EMAIL), Validators.minLength(3), Validators.maxLength(30)]],
      password: ['', [Validators.required, Validators.minLength(8)]]
   });

   //Get return URL from route parameters or default to '/'.
   this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  //Form Submitting Method.
  onSubmit() {
    //Submit Button pressed.
    this.submitted = true;

    //Stop here if form is Invalid.
    if (this.signupForm.invalid) {
      return;
    }

    //Continue if form is Valid.
    this.postUser();
  }

  //Post New User To JSON Server DataBase.
  postUser(){
    //Patching Data
    this.new_user.user_email = this.signupForm.value.email;
    this.new_user.user_password = this.signupForm.value.password;
    this.new_user.user_name = "Default User Name";
    this.new_user.users_сities = [];

    //Posting Data to JSON Server.
    this.jsonServer.createUser(this.new_user)
    .subscribe(result=>{
      console.log(result);
      alert("User Has Been Added");
    },

    //If Posting Errors.
    error=>{
      alert("Something Went Wrong");
    });
  }

  //Redirect to Login Page.
  redirect(){
    this.router.navigate(['../Log-in'], {relativeTo: this.route});
  }


}
