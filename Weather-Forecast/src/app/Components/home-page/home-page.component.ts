import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WeaterData } from 'src/app/Models/WeatherData';
import { HttpCalls } from 'src/app/Services/Open Weather API/HttpCalls';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})

//Home Page Component.
export class HomePageComponent implements OnInit {
  //Instance Attributes.
  kyiv: WeaterData;
  telAviv: WeaterData;
  moscow: WeaterData;
  washington: WeaterData;
  ottawa: WeaterData;
  beijing: WeaterData;
  bangkok: WeaterData;
  paris: WeaterData;

  //Composed Constructor.
  constructor(
    //Avalable Services.
    private router: Router,
    private route: ActivatedRoute,
    private httpService: HttpCalls
  ) {
    //Attributes initialization.
    this.kyiv = new WeaterData;
    this.telAviv = new WeaterData;
    this.moscow = new WeaterData;
    this.washington = new WeaterData;
    this.ottawa = new WeaterData;
    this.beijing = new WeaterData;
    this.bangkok = new WeaterData;
    this.paris = new WeaterData;
  }

  //Page Inintialization Method.
  ngOnInit(): void {
    this.initPageModels();
  }


  //Inintialize Home-Page Models.
  initPageModels(){
    this.httpService.getOneCityData("Kyiv")
    .subscribe(data => {
      this.kyiv.name = data['name'];
      this.kyiv.temp = (data.main['temp']-273.15).toFixed(1).toString();
      this.kyiv.lon = data.coord['lon'];
      this.kyiv.lat = data.coord['lat'];
    },
    error => console.log(error));

    this.httpService.getOneCityData("Tel Aviv")
    .subscribe(data => {
      this.telAviv.name = data['name'];
      this.telAviv.temp = (data.main['temp']-273.15).toFixed(1).toString();
      this.telAviv.lon = data.coord['lon'];
      this.telAviv.lat = data.coord['lat'];
    },
    error => console.log(error));

    this.httpService.getOneCityData("Moscow")
    .subscribe(data => {
      this.moscow.name = data['name'];
      this.moscow.temp = (data.main['temp']-273.15).toFixed(1).toString();
      this.moscow.lon = data.coord['lon'];
      this.moscow.lat = data.coord['lat'];
    },
    error => console.log(error));

    this.httpService.getOneCityData("Washington")
    .subscribe(data => {
      this.washington.name = data['name'];
      this.washington.temp = (data.main['temp']-273.15).toFixed(1).toString();
      this.washington.lon = data.coord['lon'];
      this.washington.lat = data.coord['lat'];
    },
    error => console.log(error));

    this.httpService.getOneCityData("Ottawa")
    .subscribe(data => {
      this.ottawa.name = data['name'];
      this.ottawa.temp = (data.main['temp']-273.15).toFixed(1).toString();
      this.ottawa.lon = data.coord['lon'];
      this.ottawa.lat = data.coord['lat'];
    },
    error => console.log(error));

    this.httpService.getOneCityData("Beijing")
    .subscribe(data => {
      this.beijing.name = data['name'];
      this.beijing.temp = (data.main['temp']-273.15).toFixed(1).toString();
      this.beijing.lon = data.coord['lon'];
      this.beijing.lat = data.coord['lat'];
    },
    error => console.log(error));

    this.httpService.getOneCityData("Bangkok")
    .subscribe(data => {
      this.bangkok.name = data['name'];
      this.bangkok.temp = (data.main['temp']-273.15).toFixed(1).toString();
      this.bangkok.lon = data.coord['lon'];
      this.bangkok.lat = data.coord['lat'];
    },
    error => console.log(error));

    this.httpService.getOneCityData("Paris")
    .subscribe(data => {
      this.paris.name = data['name'];
      this.paris.temp = (data.main['temp']-273.15).toFixed(1).toString();
      this.paris.lon = data.coord['lon'];
      this.paris.lat = data.coord['lat'];
    },
    error => console.log(error));
  }

}
