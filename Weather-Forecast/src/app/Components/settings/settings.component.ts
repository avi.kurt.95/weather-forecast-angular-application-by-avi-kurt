import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/Models/User';
import { WeaterData } from 'src/app/Models/WeatherData';
import { AuthenticationService } from 'src/app/Services/Auentication Service/authentication.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})

//Users Setting page Component.
export class SettingsComponent implements OnInit {
  //Instance Attributes.
  currentUser: User;
  usersWeather:Array<WeaterData>;
  weather:WeaterData;

  constructor(
    //Services
    private router: Router,
    private authService: AuthenticationService
  ) {
    //Attributes Initialization.
    this.currentUser = new User();
    this.weather = new WeaterData();
    this.usersWeather = new Array();
  }

  //On Page init Method.
  ngOnInit(): void {
    //Receive current Logged User.
    this.currentUser = this.authService.currentUserValue;

    //Push City Objects to View.
    this.currentUser.users_сities.forEach(element => {
      this.usersWeather.push(element);
    });
  }
}
