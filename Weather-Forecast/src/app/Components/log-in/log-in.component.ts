import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/Models/User';
import { AuthenticationService } from 'src/app/Services/Auentication Service/authentication.service';
import { JsonServerServiceService } from 'src/app/Services/JSON Server API/json-server-service.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})

//Login Page Component.
export class LogInComponent implements OnInit {
  //Instance Attributes.
  loginForm!: FormGroup;
  submitted = false;
  returnUrl!: string;
  logged_user:User;
  data:any;

  //Composed Constructor.
  constructor(
    //Services.
    private authentication: AuthenticationService,
    private jsonServer: JsonServerServiceService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    //Attributes Initilization.
    this.logged_user = new User();
  }

  //On Page Init Method.
  ngOnInit(): void {

    // Patterns.
    const PAT_EMAIL = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+[.][a-zA-Z]{2,4}$";

    //Login Form Builder&Validators.
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email,Validators.pattern(PAT_EMAIL), Validators.minLength(3), Validators.maxLength(30)]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });

    //Get return URL from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }


  //Form Submitting.
  onSubmit() {
    //Button Pressed.
    this.submitted = true;

    //Stop here if form is Invalid.
    if (this.loginForm.invalid) {
      return;
    }

    //Find User in DB.
    this.loginUser();
  }

  //User Authentication method.
  loginUser(){
  //Patching form Data.
  this.logged_user.user_email = this.loginForm.value.email;
  this.logged_user.user_password = this.loginForm.value.password;

  //User Authentication.
  this.authentication.login(this.logged_user.user_email, this.logged_user.user_password);
  }

  //Page Redirection Method.
  redirect(){
    this.router.navigate(['../Sign-up'], {relativeTo: this.route});
  }
}
