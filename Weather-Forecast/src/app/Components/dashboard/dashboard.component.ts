import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/Models/User';
import { WeaterData } from 'src/app/Models/WeatherData';
import { AuthenticationService } from 'src/app/Services/Auentication Service/authentication.service';
import { JsonServerServiceService } from 'src/app/Services/JSON Server API/json-server-service.service';
import { HttpCalls } from 'src/app/Services/Open Weather API/HttpCalls';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

//Users Dashboard page Component.
export class DashboardComponent implements OnInit {
  //Instance Attributes.
  searchForm!: FormGroup;
  currentUser: User;
  usersWeather:Array<WeaterData>;
  weather:WeaterData;
  inserted_name:string;

  //Composed Constructor.
  constructor(
    //Services
    private jsonServer:JsonServerServiceService,
    private authService: AuthenticationService,
    private formBuilder: FormBuilder,
    private httpCalls:HttpCalls,
    private router: Router
  ) {
    //Attributes Initialization.
    this.inserted_name = "";
    this.currentUser = new User();
    this.weather = new WeaterData();
    this.usersWeather = new Array();
  }


  ngOnInit(): void {
    //Login Form Builder&Validators.
    this.searchForm = this.formBuilder.group({
      search: ['', [Validators.required]]
    });

    //Receiving current logged User.
    this.authService.currentUser.subscribe(x => this.currentUser = x);

    //Logged User Locations.
    if(this.currentUser!=null){
      this.usersWeather = [];
      this.usersWeather = this.currentUser.users_сities;
      }
  }

  //Add new Location.
  addLocation(){
    //Clear Old Inserts.
    this.inserted_name = "";
    this.inserted_name = this.searchForm.value.search;
    this.weather = new WeaterData();

    //Receiving Single City by 'Name'.
    this.httpCalls.getOneCityData(this.inserted_name)
    .subscribe(data => {

      //Get Data from Weather Server.
      this.weather.name = data['name'];
      this.weather.temp = (data.main['temp']-273.15).toFixed(1).toString();
      this.weather.lon = data.coord['lon'];
      this.weather.lat = data.coord['lat'];

      //Pushing to View.
      this.usersWeather.push(this.weather);

      //Push to JSON server DataBase.
      this.currentUser.users_сities = this.usersWeather;
      this.jsonServer.updateUser(this.currentUser, this.currentUser.id)
        .subscribe();

      //Update Local Storege.
      localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
    },

    error => {
      //Logging Errors.
      console.log(error);
      alert("Error: City not found");
    });
  }

  //Remove Object From Array
  arrayRemove(arr:Array<WeaterData>, value:string) {
    return arr.filter(function(ele){
        return ele.name != value;
    });
  }

  //Delete Location Method.
  deleteLocation(locationName:string){

    //Remove Location
    this.usersWeather = this.arrayRemove(this.usersWeather, locationName);

    //Update User in DB.
    this.currentUser.users_сities = this.arrayRemove(this.usersWeather, locationName);
    this.jsonServer.updateUser(this.currentUser, this.currentUser.id)
    .subscribe(updatedData =>
      {this.currentUser = updatedData}
    );

    //Update Local Storage
    localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
  }
}
