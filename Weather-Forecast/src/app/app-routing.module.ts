import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutUsComponent } from './Components/about-us/about-us.component';
import { ContactComponent } from './Components/contact/contact.component';
import { DashboardComponent } from './Components/dashboard/dashboard.component';
import { HomePageComponent } from './Components/home-page/home-page.component';
import { LogInComponent } from './Components/log-in/log-in.component';
import { PageNotFoundComponent } from './Components/page-not-found/page-not-found.component';
import { SettingsComponent } from './Components/settings/settings.component';
import { SignUpComponent } from './Components/sign-up/sign-up.component';
import { UserPageComponent } from './Components/user-page/user-page.component';

const routes: Routes = [
  // Public Area.
  { path: 'Home-Page', component: HomePageComponent }, // Home Page Page.
  { path: 'About-us', component: AboutUsComponent },   // About Us Page.
  { path: 'Contact', component: ContactComponent },    // Contacts Page.
  { path: 'Sign-up', component: SignUpComponent },     // Signup Page.
  { path: 'Log-in', component: LogInComponent },       // Login Page.

  // Private/Secured Area.
  {path: 'User/:id',component: UserPageComponent,
    children: [
      { path: '', redirectTo: 'Dashboard', pathMatch: 'full' },
      { path: 'Dashboard', component: DashboardComponent }, // Users Dashboard Page.
      { path: 'Settings', component: SettingsComponent },   // Users Settings Page.
    ],
  },

  // Exceptions/Errors Area.
  { path: '404-Page-Not-Found', component: PageNotFoundComponent },
  { path: '', redirectTo: '/Home-Page', pathMatch: 'full' },
  { path: '**', redirectTo: '/404-Page-Not-Found', pathMatch: 'full' },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class AppRoutingModule { }
